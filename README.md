**Trip Buddy**

Kanika Agrawal (W1151469), Preeti Chauhan (W0877987), Haitao Huang (W01101358)


Following files are contained in this folder:

* Android Project (Zipped File) – BuddyTrip.Zip
* Web Service Code (Zipped File) - WebServiceProject.zip
* Mongo DB Collections file (Zipped file) – dump.zip
* Web Service Activator File (Zipped File) - tripbuddyws-1.0-SNAPSHOT.zip
* Demo File of our Project

How to setup the App:

1. Steps for mongodb:
     1. Install mongodb.
     1. Installation intsructions - (http://docs.mongodb.org/manual/tutorial/)
     1. Download the mongodb dump.zip file.
     1. Unzip the dump.zip.
     1. In terminal- run - mongorestore <path to the unzipped dump folder>.
     1. For more instructions, please follow - http://docs.mongodb.org/manual/tutorial/backup-with-mongodump/
     1. Start the mongo db server by running command: sudo mongod 


1. Steps for Web Service:
     1. Unzip the web service zip file - tripbuddyws-1.0-SNAPSHOT.zip.
     1. The unzipped folder will have a bin directory
     1. cd into bin directory 
     1. There are two scripts tripbuddy and tripbuddy.bat. 
     1. If you are running on linux execute in shell as ./tripbuddy or
     1. If you are on windows windows execute in command prompt tripbuddy.bat


1. Steps for the App:
     1. Unzip the BuddyTrip.zip
     1. Open it with the Android Studio
     1. In Strings.xml, update the IP address with the IP address of your machine. This is for the app to be able to communicate with the web services. 
     1. Run the App.
     1. For the App to run on mobile, you have to make sure that the App can reach the backend Web Service.

	
1. Software Requirements
    1. Facebook Android SDK version - 4.1.0 
    1. Quickblox SDK Version – 2.2.3
    1. Android SDK Version – 22
    1. Java7
    1. IntelliJ and Android Studio

Ownership of the tasks:

1. Kanika Agrawal: User Interface Design, App Code, Video Chat Integration, Text Chat Integration, Web Service and Database.
1. Preeti Chauhan:  User Interface Design.
1. Haitao Huang:  User Interface Design, Facebook Login and Integration.	
			
			
References:

* http://quickblox.com/developers/Android.
* http://developer.android.com/index.html
* https://developers.facebook.com/
* https://www.playframework.com/documentation/2.4.x/Home
* http://docs.mongodb.org/manual/