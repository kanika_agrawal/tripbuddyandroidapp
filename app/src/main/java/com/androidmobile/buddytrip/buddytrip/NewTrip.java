package com.androidmobile.buddytrip.buddytrip;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidmobile.buddytrip.buddytrip.http.CreateTripAsyncHttpUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NewTrip extends ActionBarActivity implements  TaskCallback
{
    private TextView tv_dateFrom;
    private boolean isFrom;
    private TextView tv_dateTo;
    private EditText et_title;
    private EditText et_budget;
   // private EditText et_location;
    private Spinner sp_interest;
    private Spinner sp_locations;
    private String username = "";

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_trip);

        Intent getCreateTripIntent = getIntent();
        username = getCreateTripIntent.getStringExtra("username");

        final Button btn_submit = (Button)findViewById(R.id.btn_submit);
        et_title = (EditText)findViewById(R.id.et_title);
        et_budget = (EditText)findViewById(R.id.et_budget);
        sp_interest = (Spinner) findViewById(R.id.sp_interest);
        sp_locations = (Spinner) findViewById(R.id.sp_locations);

        final Button btn_dateFrom = (Button)findViewById(R.id.btn_from);
        final Button btn_dateTo = (Button)findViewById(R.id.btn_to);
        tv_dateFrom  = (TextView)findViewById(R.id.tv_from);
        tv_dateTo  = (TextView)findViewById(R.id.tv_to);
        isFrom = true;

        btn_dateFrom.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Calendar cal = Calendar.getInstance();
                int startYear = cal.get(Calendar.YEAR);
                int startMonth = cal.get(Calendar.MONTH);
                int startDay = cal.get(Calendar.DAY_OF_MONTH);
                isFrom = true;
                DatePickerDialog dialog = new DatePickerDialog(view.getContext(), new mDateSetListener(),startYear,startMonth,startDay);
                dialog.show();
            }
        });

        btn_dateTo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Calendar cal = Calendar.getInstance();
                int startYear = cal.get(Calendar.YEAR);
                int startMonth = cal.get(Calendar.MONTH);
                int startDay = cal.get(Calendar.DAY_OF_MONTH);
                isFrom = false;
                DatePickerDialog dialog = new DatePickerDialog(view.getContext(),
                        new mDateSetListener(),startYear,startMonth,startDay);
                dialog.show();
            }
        });


        ArrayAdapter<CharSequence> interestsAdapter = ArrayAdapter.createFromResource(this,R.array.interests_array, android.R.layout.simple_spinner_item);
        interestsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_interest.setAdapter(interestsAdapter);

        ArrayAdapter<CharSequence> locationsAdapter = ArrayAdapter.createFromResource(this,R.array.locations_array, android.R.layout.simple_spinner_item);
        locationsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_locations.setAdapter(locationsAdapter);


        btn_submit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                if(et_title.getText().toString() == null || et_title.getText().toString().isEmpty()
                        || et_budget.getText().toString()==null || et_budget.getText().toString().isEmpty()
                        || tv_dateFrom.getText().toString() == null || tv_dateFrom.getText().toString().isEmpty()
                        || tv_dateTo.getText().toString() == null || tv_dateTo.getText().toString().isEmpty())
                {
                    Toast.makeText(getBaseContext(), "Please fill in all fields!!", Toast.LENGTH_SHORT).show();
                    return;
                }


                String tripTitle = et_title.getText().toString();
                String tripLocation = String.valueOf(sp_locations.getSelectedItem());
                String tripInterest = String.valueOf(sp_interest.getSelectedItem());
                Float cost = Float.parseFloat(et_budget.getText().toString());
                Date tripDateFrom =  new Date();
                Date tripDateTo = new Date();
                Date today = new Date();
                try
                {
                    tripDateFrom = sdf.parse(tv_dateFrom.getText().toString());
                    tripDateTo = sdf.parse(tv_dateTo.getText().toString());
                }
                catch (ParseException e)
                {
                    e.printStackTrace();
                }

                if(tripDateFrom.before(today))
                {
                    Toast.makeText(getBaseContext(), "Date From cannot be before Today!!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(tripDateTo.before(tripDateFrom))
                {
                    Toast.makeText(getBaseContext(), "Date To cannot be before Date From!!", Toast.LENGTH_SHORT).show();
                    return;
                }

                CreateTripAsyncHttpUtil createTripAsyncHttpUtil = new CreateTripAsyncHttpUtil(
                                tripTitle,
                                username,
                                tripDateFrom,
                                tripDateTo,
                                tripLocation,
                                tripInterest,
                                cost,
                                getBaseContext(),
                        NewTrip.this);

                createTripAsyncHttpUtil.execute();


            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_trip, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void done() {
        finish();
    }


    class mDateSetListener implements DatePickerDialog.OnDateSetListener
    {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int day)
        {
            String date = day + "/" + (month + 1) + "/" + year;
            if(isFrom)
            {
                tv_dateFrom.setText(date);
            }
            else
            {
                tv_dateTo.setText(date);
            }
        }
    }
}
