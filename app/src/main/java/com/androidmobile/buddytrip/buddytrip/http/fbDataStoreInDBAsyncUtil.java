package com.androidmobile.buddytrip.buddytrip.http;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.androidmobile.buddytrip.buddytrip.R;
import com.androidmobile.buddytrip.buddytrip.UserHomePage;
import com.androidmobile.buddytrip.buddytrip.utils.StringUtils;
import com.quickblox.auth.QBAuth;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.net.URI;

public class FBDataStoreInDBAsyncUtil extends AsyncTask<Void, Void, Void>
{
    private final Context context;
    private final HttpClient httpClient;
    private JSONObject jsonObject;

    private boolean openUserHomePage;
    private String userName;
    private boolean isCreated;


    public FBDataStoreInDBAsyncUtil(JSONObject jsonObject, Context context)
    {
        this.context = context;
        this.httpClient = new DefaultHttpClient();
        this.openUserHomePage = false;
        this.jsonObject=jsonObject;
        this.isCreated=false;
    }

    @Override
    protected Void doInBackground(Void ... voidParams)
    {
        try
        {
            userName = jsonObject.getString("_id");
            String serviceURL = context.getString(R.string.URL);

            HttpGet getUserRequest = new HttpGet();
            getUserRequest.setURI(new URI(serviceURL + "/fbusers/" + userName));

            HttpResponse response = httpClient.execute(getUserRequest);
            if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
            {
                openUserHomePage = true;
                jsonObject = new JSONObject(StringUtils.inputStreamToString(response.getEntity().getContent()));
            }
            else if (response.getStatusLine().getStatusCode() == HttpStatus.SC_NOT_FOUND)
            {
                QBAuth.createSession();
                QBUser createdQBUser = QBUsers.signUp(new QBUser(userName, "nopassword"));
                jsonObject.put("QBId", createdQBUser.getId());

                HttpPost postCreateUserRequest = new HttpPost();
                postCreateUserRequest.setURI(new URI(serviceURL + "/users/" + userName));
                postCreateUserRequest.setEntity(new StringEntity(jsonObject.toString()));
                postCreateUserRequest.setHeader("Content-type", "application/json");

                HttpResponse httpResponse = httpClient.execute(postCreateUserRequest);
                if(httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED)
                {
                    isCreated = true;
                    openUserHomePage = true;
                }
                else if(httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_BAD_REQUEST)
                {
                    isCreated = false;
                    openUserHomePage = true;
                    Toast.makeText(context, "Username already exists. Try Again.", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    isCreated = false;
                    Toast.makeText(context, "Unexpected error. Try Again.", Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                openUserHomePage = false;
            }
        }
        catch(Exception ex)
        {
            openUserHomePage = false;
            Toast.makeText(context, "Unexpected error. Try Again.", Toast.LENGTH_SHORT).show();
        }

        return null;
    }
    @Override
    protected void onPostExecute(Void aVoid)
    {
        if (openUserHomePage)
        {
            Intent userHomePageIntent = new Intent(context, UserHomePage.class);
            userHomePageIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            userHomePageIntent.putExtra("JSONObject", jsonObject.toString());
            context.startActivity(userHomePageIntent);
        }
        else
        {
            Toast.makeText(context, "Error Occurred. Please try again!", Toast.LENGTH_SHORT).show();
        }
    }

}
