package com.androidmobile.buddytrip.buddytrip.utils;

import com.androidmobile.buddytrip.buddytrip.R;

/**
 * Created by kanikaagrawal on 5/29/15.
 */
public enum LocationImageMapper
{
    BOSTON("Boston", R.drawable.boston),
    CHICAGO("Chicago", R.drawable.chicago),
    HAWAII("Hawaii", R.drawable.hawaii),
    HONOLULU("Honolulu", R.drawable.honolulu),
    HOUSTON("Houston", R.drawable.houston),
    KAUAI("Kauai", R.drawable.kauai),
    KEY_WEST("Key West", R.drawable.key_west),
    LAS_VEGAS("Las Vegas", R.drawable.las_vegas),
    LOS_ANGELES("los_angeles", R.drawable.los_angeles),
    MAUI("Maui", R.drawable.maui),
    MEMPHIS("Memphis", R.drawable.memphis),
    MIAMI("Miami", R.drawable.miami),
    NASHVILLE("Nashville", R.drawable.nashville),
    NEW_ORLEANS("New Orleans", R.drawable.neworleans),
    NEW_YORK("New York", R.drawable.newyork),
    OAHU("Oahu",R.drawable.oahu),
    ORLANDO("Orlando", R.drawable.orlando),
    PALM_SPRINGS("Palm Springs", R.drawable.palm_springs),
    PHILADELPHIA("Philadelphia", R.drawable.philadelphia),
    PORTLAND("Portland", R.drawable.portland),
    SAN_ANTONIO("San Antonio", R.drawable.san_antonio),
    SAN_DIEGO("San Diego", R.drawable.san_diego),
    SAN_FRANCISCO("San Francisco", R.drawable.san_francisco),
    SANTA_MONICA("Santa Monica", R.drawable.santa_monica),
    SAVANNAH("Savannah", R.drawable.savannah),
    SEATTLE("Seattle", R.drawable.seattle),
    SEDONA("Sedona",R.drawable.sedona),
    WASHINGTON("Washington D.C.", R.drawable.washington),
    YELLOWSTONE_NATIONAL_PARK("Yellowstone National Park", R.drawable.yellowstone_national_park),
    YOSEMITE_NATIONAL_PARK("Yosemite National Park", R.drawable.yosemite),
    NOT_MAPPED("Not Mapped", R.drawable.camera);

    public final String location;
    public final int drawable;

    private LocationImageMapper(final String location, final int drawable)
    {
        this.location = location;
        this.drawable = drawable;
    }

    public static int getDrawableFor(final String locationInput)
    {
        for(LocationImageMapper current : LocationImageMapper.values())
        {
            if(current.location.equalsIgnoreCase(locationInput))
                return current.drawable;
        }

        return LocationImageMapper.NOT_MAPPED.drawable;
    }

}
