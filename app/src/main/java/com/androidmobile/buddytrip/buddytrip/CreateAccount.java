package com.androidmobile.buddytrip.buddytrip;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.androidmobile.buddytrip.buddytrip.http.CreateAccountAsyncHttpUtil;

public class CreateAccount extends ActionBarActivity
{
    private EditText etName;
    private EditText etUsername;
    private EditText etPassword;
    private EditText etConfirmPassword;
    private EditText etAge;
    private Button btnSignUp;
    private String gender;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_create_account);
        etName = (EditText)findViewById(R.id.et_name);
        etUsername = (EditText)findViewById(R.id.et_username);
        etPassword = (EditText)findViewById(R.id.et_password);
        etConfirmPassword = (EditText)findViewById(R.id.et_confirm_password);
        etAge = (EditText)findViewById(R.id.et_age);
        btnSignUp = (Button)findViewById(R.id.btn_signup);
        gender = "Male";


        final RadioGroup rgGender = (RadioGroup) findViewById(R.id.rGroup);
        rgGender.setOnCheckedChangeListener(
                new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(final RadioGroup group, final int checkedId)
                    {
                        final RadioButton rbMale = (RadioButton) findViewById(R.id.radioButtonMale);
                        final RadioButton rbFemale = (RadioButton) findViewById(R.id.radioButtonFemale);


                        if (rbFemale.isChecked())
                        {
                            gender = "Female";
                        }
                        else
                        {
                            gender = "Male";
                        }
                    }
                });



        btnSignUp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Field Validations
                String name = etName.getText().toString();
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();
                String confirmPassword = etConfirmPassword.getText().toString();
                String age = etAge.getText().toString();


                if (name == null || name.isEmpty() ||
                        username == null || username.isEmpty() ||
                        password == null || password.isEmpty() ||
                        confirmPassword == null || confirmPassword.isEmpty() ||
                         age == null) {
                    Toast.makeText(getBaseContext(), "Please fill all the fields!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!etPassword.getText().toString().equals(etConfirmPassword.getText().toString())) {
                    Toast.makeText(getBaseContext(), "Passwords do not match", Toast.LENGTH_SHORT).show();

                    return;
                }

                CreateAccountAsyncHttpUtil createAccountAsync =
                        new CreateAccountAsyncHttpUtil(
                                etUsername.getText().toString(),
                                etName.getText().toString(),
                                etPassword.getText().toString(),
                                gender,
                                Integer.parseInt(etAge.getText().toString()),
                                getBaseContext());

                createAccountAsync.execute();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
