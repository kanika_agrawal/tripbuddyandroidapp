package com.androidmobile.buddytrip.buddytrip;

import java.io.Serializable;

/**
 * Created by huanhait on 5/14/15.
 */
public class Person implements Serializable {
    private static final long serialVersionUID = 1L;
    private String strUserName;
    private String strPwd;
    private String strGender;
    private String strName;
    private int age;


    public void setName(String userName) {
        strName = userName;
    }

    public String getName() {
        return strName;
    }

    public void setAge(int userAge) {
        age = userAge;
    }

    public Integer getSge() {
        return age;
    }

    public void setUserName(String userName) {
        strUserName = userName;
    }

    public String getUserName() {
        return strUserName;
    }

    public void setPassword(String password) {
        strPwd = password;
    }

    public String getPassword() {
        return strPwd;
    }

    public void setGender(String gender) {
        strGender = gender;
    }

    public String getGender() {
        return strGender;
    }
}

