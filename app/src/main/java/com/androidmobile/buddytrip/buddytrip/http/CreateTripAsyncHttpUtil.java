package com.androidmobile.buddytrip.buddytrip.http;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.androidmobile.buddytrip.buddytrip.R;
import com.androidmobile.buddytrip.buddytrip.TaskCallback;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.net.URI;
import java.util.Date;

public class CreateTripAsyncHttpUtil extends AsyncTask<Void, Void, Void>
{
    private final String tripTitle;
    private final String username;
    private final Date dateFrom;
    private final Date dateTo;
    private final String location;
    private final String interest;
    private final float cost;
    private final Context context;
    private final TaskCallback callback;

    private final HttpClient httpClient;

    private boolean isCreated;

    public CreateTripAsyncHttpUtil(
            String tripTitle,
            String userName,
            Date dateFrom,
            Date dateTo,
            String location,
            String interest,
            float cost,
            Context context,
            TaskCallback callback)
    {
        this.tripTitle = tripTitle;
        this.username = userName;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.location = location;
        this.interest = interest;
        this.cost = cost;
        this.context = context;
this.callback = callback;
        httpClient = new DefaultHttpClient();

        isCreated = false;
    }

    @Override
    protected Void doInBackground(Void ... voidParams)
    {
        try
        {
            JSONObject tripDetails = new JSONObject();
            tripDetails.put("tripTitle", tripTitle);
            tripDetails.put("username", username);
            tripDetails.put("dateFrom", dateFrom.toString());
            tripDetails.put("dateTo", dateTo.toString());
            tripDetails.put("location", location);
            tripDetails.put("interest", interest);
            tripDetails.put("cost", String.valueOf(cost));

            String serviceURL = context.getString(R.string.URL);

            HttpPost postCreateUserRequest = new HttpPost();
            postCreateUserRequest.setURI(new URI(serviceURL + "/trips"));
            postCreateUserRequest.setEntity(new StringEntity(tripDetails.toString()));
            postCreateUserRequest.setHeader("Content-type", "application/json");

            HttpResponse response = httpClient.execute(postCreateUserRequest);
            if(response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED)
            {
                isCreated = true;
            }
            else
            {
                isCreated = false;
                Toast.makeText(context, "Unexpected error. Try Again.", Toast.LENGTH_SHORT).show();
            }
        }
        catch(Exception ex)
        {
            isCreated = false;
            Toast.makeText(context, "Unexpected error. Try Again.", Toast.LENGTH_SHORT).show();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid)
    {
        if(isCreated)
        {
            callback.done();
        }
        else
        {
            Toast.makeText(context, "Unexpected Error happened...Try again!!", Toast.LENGTH_SHORT).show();
        }
    }
}
