package com.androidmobile.buddytrip.buddytrip;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidmobile.buddytrip.buddytrip.utils.DialogHelper;
import com.androidmobile.buddytrip.buddytrip.utils.OnCallDialogListener;
import com.androidmobile.buddytrip.buddytrip.views.OpponentSurfaceView;
import com.androidmobile.buddytrip.buddytrip.views.OwnSurfaceView;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.core.QBVideoChatController;
import com.quickblox.videochat.model.listeners.OnQBVideoChatListener;
import com.quickblox.videochat.model.objects.CallState;
import com.quickblox.videochat.model.objects.CallType;
import com.quickblox.videochat.model.objects.VideoChatConfig;

import org.jivesoftware.smack.XMPPException;


public class VideoChatActivity extends ActionBarActivity
{

    private OwnSurfaceView myView;
    private OpponentSurfaceView opponentView;

    private ProgressBar progressBar;
    private Button switchCameraButton;
    private Button startStopVideoCallBtn;
    private TextView txtName;

    private AlertDialog alertDialog;

    private VideoChatConfig videoChatConfig;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_chat);

        initViews();
    }

    private void initViews()
    {
        // VideoChat settings
        videoChatConfig = getIntent().getParcelableExtra(VideoChatConfig.class.getCanonicalName());


        // Setup UI
        opponentView = (OpponentSurfaceView) findViewById(R.id.opponentView);

        switchCameraButton = (Button)findViewById(R.id.switch_camera_button);

        switchCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myView.switchCamera();
            }
        });

        myView = (OwnSurfaceView) findViewById(R.id.ownCameraView);

        myView.setCameraDataListener(new OwnSurfaceView.CameraDataListener() {
            @Override
            public void onCameraDataReceive(byte[] data) {
                if (videoChatConfig != null && videoChatConfig.getCallType() != CallType.VIDEO_AUDIO) {
                    return;
                }
                QBVideoChatController.getInstance().sendVideo(data);
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.opponentImageLoading);

        final Intent intent = getIntent();

        startStopVideoCallBtn = (Button) findViewById(R.id.startStopCallBtn);
        startStopVideoCallBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Button btn = (Button)v;
                // Call
                if(btn.getText().equals("Call"))
                {
                    progressBar.setVisibility(View.VISIBLE);


                    QBUser opponentUser = new QBUser(intent.getStringExtra("FriendUsername"),"nopassword");
                    opponentUser.setId(intent.getIntExtra("FriendQBId", 0));

                    videoChatConfig = QBVideoChatController.getInstance().callFriend(opponentUser, CallType.VIDEO_AUDIO, null);

                    // Stop call
                }
                else
                {
                    startStopVideoCallBtn.setText("Call");
                    progressBar.setVisibility(View.INVISIBLE);

                    QBVideoChatController.getInstance().finishVideoChat(videoChatConfig);

                    opponentView.clear();
                }
            }
        });

        // Set video chat listener
        //
        try
        {
            QBUser currentQBUser = new QBUser(intent.getStringExtra("MyUsername"), "nopassword");
            QBVideoChatController.getInstance().setQBVideoChatListener(currentQBUser, qbVideoChatListener);
        }
        catch (XMPPException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_video_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    OnQBVideoChatListener qbVideoChatListener = new OnQBVideoChatListener() {

        @Override
        public void onCameraDataReceive(byte[] videoData) {
            //
        }

        @Override
        public void onMicrophoneDataReceive(byte[] audioData) {
            QBVideoChatController.getInstance().sendAudio(audioData);
        }

        @Override
        public void onOpponentVideoDataReceive(final byte[] videoData) {
            opponentView.render(videoData);
        }

        @Override
        public void onOpponentAudioDataReceive(byte[] audioData) {
            QBVideoChatController.getInstance().playAudio(audioData);
        }

        @Override
        public void onProgress(boolean progress) {
//            progressBar.setVisibility(progress ? View.VISIBLE : View.GONE);
        }

        @Override
        public void onVideoChatStateChange(CallState callState, VideoChatConfig receivedVideoChatConfig) {
            videoChatConfig = receivedVideoChatConfig;

            switch (callState) {
                case ON_CALL_START:
                    Toast.makeText(getBaseContext(), "Call Started", Toast.LENGTH_SHORT).show();

                    progressBar.setVisibility(View.INVISIBLE);
                    break;
                case ON_CANCELED_CALL:
                    Toast.makeText(getBaseContext(), "Call Cancelled", Toast.LENGTH_SHORT).show();

                    videoChatConfig = null;
                    if (alertDialog != null && alertDialog.isShowing()){
                        alertDialog.dismiss();
                    }
                    autoCancelHandler.removeCallbacks(autoCancelTask);

                    break;
                case ON_CALL_END:
                    Toast.makeText(getBaseContext(), "Call Ended", Toast.LENGTH_SHORT).show();

                    // clear opponent view
                    opponentView.clear();
                    startStopVideoCallBtn.setText("Call");
                    break;
                case ACCEPT:
                    Toast.makeText(getBaseContext(), "Accepted", Toast.LENGTH_SHORT).show();

                    showIncomingCallDialog();
                    break;
                case ON_ACCEPT_BY_USER:
                    Toast.makeText(getBaseContext(), "Call Accepted", Toast.LENGTH_SHORT).show();

                    QBVideoChatController.getInstance().onAcceptFriendCall(videoChatConfig, null);
                    break;
                case ON_REJECTED_BY_USER:
                    Toast.makeText(getBaseContext(), "Call Rejected", Toast.LENGTH_SHORT).show();

                    progressBar.setVisibility(View.INVISIBLE);
                    break;
                case ON_CONNECTED:
                    Toast.makeText(getBaseContext(), "Connected", Toast.LENGTH_SHORT).show();

                    progressBar.setVisibility(View.INVISIBLE);

                    startStopVideoCallBtn.setText("Hang up");
                    break;
                case ON_START_CONNECTING:
                    Toast.makeText(getBaseContext(), "Establishing Connection", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };


    private Handler autoCancelHandler = new Handler(Looper.getMainLooper());
    private Runnable autoCancelTask = new Runnable() {
        @Override
        public void run() {
            if (alertDialog != null && alertDialog.isShowing()){
                alertDialog.dismiss();
            }
        }
    };

    private void showIncomingCallDialog() {
        alertDialog = DialogHelper.showCallDialog(this, new OnCallDialogListener() {
            @Override
            public void onAcceptCallClick() {
                progressBar.setVisibility(View.VISIBLE);

                QBVideoChatController.getInstance().acceptCallByFriend(videoChatConfig, null);

                autoCancelHandler.removeCallbacks(autoCancelTask);
            }

            @Override
            public void onRejectCallClick() {
                QBVideoChatController.getInstance().rejectCall(videoChatConfig);

                autoCancelHandler.removeCallbacks(autoCancelTask);
            }
        });

        autoCancelHandler.postDelayed(autoCancelTask, 30000);
    }
}

