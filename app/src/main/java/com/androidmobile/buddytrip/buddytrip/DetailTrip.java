package com.androidmobile.buddytrip.buddytrip;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.androidmobile.buddytrip.buddytrip.http.FindFriendsAsyncHttpUtil;
import com.androidmobile.buddytrip.buddytrip.http.LoginAsyncHttpUtil;

import java.text.SimpleDateFormat;
import java.util.Date;


public class DetailTrip extends ActionBarActivity
{
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_trip);

        Intent tripDetailIntent = getIntent();
        final String tripTitle  = tripDetailIntent.getStringExtra("Trip Title");
        final String tripLocation  = tripDetailIntent.getStringExtra("Trip Location");
        final String tripInterest = tripDetailIntent.getStringExtra("Trip Interest");
        final Float tripCost =  tripDetailIntent.getFloatExtra("Trip Cost", 0);
        final Date tripDateFrom  = (Date)tripDetailIntent.getSerializableExtra("Trip Date From");
        final Date tripDateTo  = (Date)tripDetailIntent.getSerializableExtra("Trip Date To");
        final Integer tripID =  tripDetailIntent.getIntExtra("Trip ID", 0);
        final String username = tripDetailIntent.getStringExtra("Trip Username");
        final Integer qbId = tripDetailIntent.getIntExtra("QBId", 0);

        //Display the value to the screen
        final TextView displayTripLocation = (TextView) findViewById(R.id.tv_location);
        displayTripLocation.setText(tripLocation);

        final TextView displayTripInterest = (TextView) findViewById(R.id.tv_interest);
        displayTripInterest.setText(tripInterest);

        final TextView displayTripTitle = (TextView) findViewById(R.id.tv_title);
        displayTripTitle.setText(tripTitle);

        final TextView displayTripCost = (TextView) findViewById(R.id.tv_budget1);
        displayTripCost.setText(tripCost.toString());

        final TextView tv_dateFrom  = (TextView)findViewById(R.id.tv_from);
        tv_dateFrom.setText(sdf.format(tripDateFrom).toString());

        final TextView tv_dateTo = (TextView)findViewById(R.id.tv_to);
        tv_dateTo.setText(sdf.format(tripDateTo).toString());


       Button btn_findFriends = (Button) findViewById(R.id.btn_findFriends);
       btn_findFriends.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                try
                {
                    FindFriendsAsyncHttpUtil findFriendsAsyncHttpUtil = new FindFriendsAsyncHttpUtil(getBaseContext(), tripID, username, qbId);

                    findFriendsAsyncHttpUtil.execute();
                }
                catch (Exception e)
                {
                    Log.d("HTTPCLIENTFETCHDATA", e.getLocalizedMessage());
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail_trip, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
