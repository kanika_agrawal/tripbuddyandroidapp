package com.androidmobile.buddytrip.buddytrip;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidmobile.buddytrip.buddytrip.utils.DateUtils;
import com.androidmobile.buddytrip.buddytrip.utils.LocationImageMapper;
import com.androidmobile.buddytrip.buddytrip.utils.QuickBloxAccountDetails;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBSettings;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.core.QBVideoChatController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class FriendList extends ActionBarActivity
{
    ArrayList<Friends> friends;
    ListView friendsListView;
    String username;
    Integer qbId;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friendlist);

        Intent intent = getIntent();
        username = intent.getStringExtra("username");
        qbId = intent.getIntExtra("QBId", 0);
        String jsonArrayString = intent.getStringExtra("FriendsJsonArray");

        friends = new ArrayList<>();

        try
        {
            JSONArray jsonArray = new JSONArray(jsonArrayString);
            for(int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject object = jsonArray.getJSONObject(i);
                Integer tripId = Integer.parseInt(object.getString("_id"));
                Float cost = Float.parseFloat(object.getString("cost"));

                Date dateFrom  = DateUtils.getDateFromJsonObject(object.getString("dateFrom"));
                Date dateTo = DateUtils.getDateFromJsonObject(object.getString("dateTo"));

                Integer friendsQbId = Integer.parseInt(object.getString("QBId"));

                if(dateFrom == null || dateTo == null)
                    continue;

                friends.add(new Friends(tripId,
                        object.getString("tripTitle"),
                        object.getString("username"),
                        object.getString("location"),
                        object.getString("interest"),
                        cost,
                        dateFrom,
                        dateTo,
                        object.getString("name"),
                        friendsQbId));
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        populateListView();
    }

    private void populateListView()
    {
        final ArrayAdapter<Friends> adapter = new UserFriendListAdapter();
        friendsListView = (ListView)findViewById(R.id.lv_friends);
        friendsListView.setAdapter(adapter);
    }

    private class UserFriendListAdapter extends ArrayAdapter<Friends>
    {
        public UserFriendListAdapter()
        {
            super(FriendList.this, R.layout.list_row_friends, friends);
        }

        public View getView(final int position, final View convertView, final ViewGroup parent)
        {
            //Make sure we have a view to work with
            View itemView = convertView;
            if(itemView == null)
            {
                itemView = getLayoutInflater().inflate(R.layout.list_row_friends, parent, false);
            }

            //Find the Trips to work with
            final Friends currentFriend = friends.get(position);
            final Integer friendQBId = currentFriend.getQbId();
            final String friendUsername = currentFriend.getFriendUsername();
            final String friendName = currentFriend.getFriendName();


            final TextView nameText = (TextView)itemView.findViewById(R.id.txtFriend);
            nameText.setText(currentFriend.getFriendName());

            //Trip Name
            final Button textChatBtn = (Button) itemView.findViewById(R.id.txtChat);
            textChatBtn.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    ChatService.initIfNeed(getBaseContext());

                    QBSettings.getInstance().fastConfigInit(
                            QuickBloxAccountDetails.APP_ID,
                            QuickBloxAccountDetails.AUTH_KEY,
                            QuickBloxAccountDetails.AUTH_SECRET);

                    ChatService.getInstance().logout();

                    QBUser currentQBUser = new QBUser(username, "nopassword");


                    ChatService.getInstance().login(currentQBUser, new QBEntityCallbackImpl()
                    {
                        @Override
                        public void onSuccess()
                        {
                            try {
                                Intent chatActivityIntent = new Intent(getBaseContext(), ChatActivity.class);
                                chatActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                chatActivityIntent.putExtra("FriendQBId", friendQBId);
                                chatActivityIntent.putExtra("FriendUsername", friendUsername);
                                chatActivityIntent.putExtra("MyUsername", username);
                                chatActivityIntent.putExtra("MyQBId", qbId);
                                chatActivityIntent.putExtra("FriendName", friendName);

                                QBRequestGetBuilder customObjectRequestBuilder = new QBRequestGetBuilder();
                                customObjectRequestBuilder.setPagesLimit(100);

                                List<QBDialog> dialogs = QBChatService.getChatDialogs(QBDialogType.PRIVATE, customObjectRequestBuilder, new Bundle());

                                for(QBDialog currentDialog : dialogs)
                                {
                                    if(currentDialog.getOccupants().contains(friendQBId))
                                    {
                                        chatActivityIntent.putExtra("FriendQBDialog", currentDialog);
                                        break;
                                    }
                                }

                                startActivity(chatActivityIntent);
                            }
                            catch (Exception ex)
                            {
                                Toast.makeText(getBaseContext(), "Text chat failed. Try again.", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError(List errors)
                        {
                            Toast.makeText(getBaseContext(), "Text chat failed. Try again.", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            });

            final Button vidChatBtn = (Button) itemView.findViewById(R.id.vidChat);
            vidChatBtn.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    ChatService.initIfNeed(getBaseContext());

                    QBSettings.getInstance().fastConfigInit(
                            QuickBloxAccountDetails.APP_ID,
                            QuickBloxAccountDetails.AUTH_KEY,
                            QuickBloxAccountDetails.AUTH_SECRET);

                    ChatService.getInstance().logout();

                    QBUser currentQBUser = new QBUser(username, "nopassword");

                    ChatService.getInstance().login(currentQBUser, new QBEntityCallbackImpl()
                    {
                        @Override
                        public void onSuccess()
                        {
                            try {

                                QBVideoChatController.getInstance().initQBVideoChatMessageListener();

                                Intent videoChatActivityIntent = new Intent(getBaseContext(), VideoChatActivity.class);
                                videoChatActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                videoChatActivityIntent.putExtra("FriendQBId", friendQBId);
                                videoChatActivityIntent.putExtra("FriendUsername", friendUsername);
                                videoChatActivityIntent.putExtra("MyUsername", username);
                                videoChatActivityIntent.putExtra("MyQBId", qbId);
                                videoChatActivityIntent.putExtra("FriendName", friendName);

                                startActivity(videoChatActivityIntent);
                            }
                            catch (Exception ex)
                            {
                                Toast.makeText(getBaseContext(), "Video chat failed. Try again.", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError(List errors)
                        {
                            System.out.print(errors.toString());
                            Toast.makeText(getBaseContext(), "Video chat failed. Try again.", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            });



            return itemView;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_friendlist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
