package com.androidmobile.buddytrip.buddytrip;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.request.QBRequestGetBuilder;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ChatActivity extends ActionBarActivity
{
    public static final String EXTRA_DIALOG = "dialog";
    private final String PROPERTY_SAVE_TO_HISTORY = "save_to_history";

    private EditText messageEditText;
    private ListView messagesContainer;
    private Button sendButton;
    private ProgressBar progressBar;
    private ChatAdapter adapter;

    private Chat chat;
    private QBDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        initViews();

        initChat();

        ChatService.getInstance().addConnectionListener(chatConnectionListener);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initViews()
    {
        messagesContainer = (ListView) findViewById(R.id.messagesContainer);
        messageEditText = (EditText) findViewById(R.id.messageEdit);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        TextView companionLabel = (TextView) findViewById(R.id.companionLabel);

        // Setup opponents info
        //
        Intent intent = getIntent();
        dialog = (QBDialog)intent.getSerializableExtra("FriendQBDialog");

        Integer opponentID = intent.getIntExtra("FriendQBId", 0);
        String friendName = intent.getStringExtra("FriendName");

        companionLabel.setText(friendName);

        // Send button
        //
        sendButton = (Button) findViewById(R.id.chatSendButton);
        sendButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String messageText = messageEditText.getText().toString();
                if (TextUtils.isEmpty(messageText))
                {
                    return;
                }

                // Send chat message
                //
                QBChatMessage chatMessage = new QBChatMessage();
                chatMessage.setBody(messageText);
                chatMessage.setProperty(PROPERTY_SAVE_TO_HISTORY, "1");
                chatMessage.setDateSent(new Date().getTime() / 1000);

                try
                {
                    chat.sendMessage(chatMessage);
                }
                catch (Exception ex)
                {
                    Toast.makeText(getBaseContext(), "Message send failed. Try again.", Toast.LENGTH_SHORT).show();
                }

                messageEditText.setText("");

                showMessage(chatMessage);
            }
        });
    }

    public void showMessage(QBChatMessage message)
    {
        adapter.add(message);

        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                adapter.notifyDataSetChanged();
                scrollDown();
            }
        });
    }

    private void scrollDown() {
        messagesContainer.setSelection(messagesContainer.getCount() - 1);
    }

    private void initChat()
    {

        Intent intent = getIntent();
        Integer opponentID = intent.getIntExtra("FriendQBId", 0);

        chat = new PrivateChatImpl(this, opponentID);

        adapter = new ChatAdapter(ChatActivity.this, new ArrayList<QBChatMessage>());
        messagesContainer.setAdapter(adapter);

        loadChatHistory();
    }

    private void loadChatHistory()
    {
        QBRequestGetBuilder customObjectRequestBuilder = new QBRequestGetBuilder();
        customObjectRequestBuilder.setPagesLimit(100);
        customObjectRequestBuilder.sortDesc("date_sent");

        QBChatService.getDialogMessages(dialog, customObjectRequestBuilder, new QBEntityCallbackImpl<ArrayList<QBChatMessage>>() {
            @Override
            public void onSuccess(ArrayList<QBChatMessage> messages, Bundle args) {

                for (int i = messages.size() - 1; i >= 0; --i) {
                    QBChatMessage msg = messages.get(i);
                    showMessage(msg);
                }

                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(List<String> errors)
            {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    ConnectionListener chatConnectionListener = new ConnectionListener() {
        @Override
        public void connected(XMPPConnection connection) {
            System.out.println("connected");
        }

        @Override
        public void authenticated(XMPPConnection connection) {
            System.out.println("authenticated");
        }

        @Override
        public void connectionClosed() {
            System.out.println("connectionClosed");
        }

        @Override
        public void connectionClosedOnError(final Exception e)
        {
            System.out.println("connectionClosedOnError: " + e.getLocalizedMessage());
        }

        @Override
        public void reconnectingIn(final int seconds) {
            if(seconds % 5 == 0) {
                System.out.println("reconnectingIn: " + seconds);
            }
        }

        @Override
        public void reconnectionSuccessful() {
            System.out.println("reconnectionSuccessful");

        }

        @Override
        public void reconnectionFailed(final Exception error) {
            System.out.println("reconnectionFailed: " + error.getLocalizedMessage());
        }
    };
}
