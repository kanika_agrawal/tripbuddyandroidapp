package com.androidmobile.buddytrip.buddytrip;

import java.util.Date;

public class TripUtil
{
    private String tripTitle;
    private String TripLocation;
    private String tripInterest;
    private float tripCost;
    private Date tripDateFrom;
    private Date tripDateTo;
    private int tripId;
    private String userName;


    public TripUtil(Integer tripId, String tripTitle, String userName, String location, String interest, Float cost, Date dateFrom, Date dateTo)
    {
        this.tripTitle = tripTitle;
        this.TripLocation = location;
        this.tripInterest = interest;
        this.tripCost = cost;
        this.tripDateFrom = dateFrom;
        this.tripDateTo = dateTo;
        this.tripId = tripId;
        this.userName = userName;
    }

    public void setTripTitle(String tripTitle)
    {
        tripTitle = tripTitle;
    }

    public String getTripTitle()
    {
        return tripTitle;
    }

    public void setTripLocation(String location)
    {
        location = location;
    }

    public String getTripLocation()
    {
        return TripLocation;
    }

    public void setTripInterest(String interest)
    {
        interest = interest;
    }

    public String getTripInterest()
    {
        return tripInterest;
    }

    public Integer getTripId()
    {
        return tripId;
    }

    public Float getTripCost()
    {
        return tripCost;
    }

    public String getTripUsername()
    {
        return userName;
    }

    public Date getTripDateFrom()
    {
        return tripDateFrom;
    }

    public Date getTripDateTo()
    {
        return tripDateTo;
    }

    @Override
    public String toString() {
        return this.tripTitle ;
    }
}
