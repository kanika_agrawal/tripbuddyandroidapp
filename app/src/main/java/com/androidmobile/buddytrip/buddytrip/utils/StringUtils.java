package com.androidmobile.buddytrip.buddytrip.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by kanikaagrawal on 5/19/15.
 */
public class StringUtils
{
    public static String inputStreamToString(final InputStream inputStream) throws IOException
    {
        if(inputStream == null)
            return "";

        StringBuilder builder = new StringBuilder();

        BufferedReader reader =
                new BufferedReader(
                        new InputStreamReader(inputStream),
                        65728);

        String line = null;

        while ((line = reader.readLine()) != null)
        {
            builder.append(line);
        }

        return builder.toString();
    }
}
