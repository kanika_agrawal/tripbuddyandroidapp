package com.androidmobile.buddytrip.buddytrip.http;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.androidmobile.buddytrip.buddytrip.Login;
import com.androidmobile.buddytrip.buddytrip.R;
import com.quickblox.auth.QBAuth;
import com.quickblox.core.QBSettings;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.net.URI;

public class CreateAccountAsyncHttpUtil extends AsyncTask<Void, Void, Void>
{
    private final String userName;
    private final String name;
    private final String password;
    private final String gender;
    private final int age;
    private final Context context;

    private boolean isCreated = false;

    private final HttpClient httpClient;

    public CreateAccountAsyncHttpUtil(String username, String name, String password, String gender, int age, Context context)
    {
        this.userName = username;
        this.name = name;
        this.password = password;
        this.gender = gender;
        this.age = age;
        this.context = context;

        this.httpClient = new DefaultHttpClient();
    }

    @Override
    protected Void doInBackground(Void ... voidParams)
    {
        try
        {
            JSONObject userDetails = new JSONObject();
            userDetails.put("password", password);
            userDetails.put("gender", gender);
            userDetails.put("age", String.valueOf(age));
            userDetails.put("name", name);

            String serviceURL = context.getString(R.string.URL);

            QBAuth.createSession();
            QBUser createdQBUser = QBUsers.signUp(new QBUser(userName, "nopassword"));
            userDetails.put("QBId", createdQBUser.getId());

            HttpPost postCreateUserRequest = new HttpPost();
            postCreateUserRequest.setURI(new URI(serviceURL + "/users/" + userName));
            postCreateUserRequest.setEntity(new StringEntity(userDetails.toString()));
            postCreateUserRequest.setHeader("Content-type", "application/json");

            HttpResponse response = httpClient.execute(postCreateUserRequest);
            if(response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED)
            {
                isCreated = true;
            }
            else if(response.getStatusLine().getStatusCode() == HttpStatus.SC_BAD_REQUEST)
            {
                isCreated = false;
                Toast.makeText(context, "Username already exists. Try Again.", Toast.LENGTH_SHORT).show();
            }
            else
            {
                isCreated = false;
                Toast.makeText(context, "Unexpected error. Try Again.", Toast.LENGTH_SHORT).show();
            }
        }
        catch(Exception ex)
        {
            isCreated = false;
            Toast.makeText(context, "Unexpected error. Try Again.", Toast.LENGTH_SHORT).show();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid)
    {
        if(isCreated)
        {
            Toast.makeText(context, "User created. Please login.", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(context, Login.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            context.startActivity(intent);
        }
    }
}
