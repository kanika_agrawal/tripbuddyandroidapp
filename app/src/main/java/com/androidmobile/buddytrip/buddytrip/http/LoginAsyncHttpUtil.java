package com.androidmobile.buddytrip.buddytrip.http;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.androidmobile.buddytrip.buddytrip.R;
import com.androidmobile.buddytrip.buddytrip.UserHomePage;
import com.androidmobile.buddytrip.buddytrip.utils.StringUtils;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.net.URI;

public class LoginAsyncHttpUtil extends AsyncTask<Void, Void, Void>
{
    private final String userName;
    private final String password;
    private final Context context;

    private final HttpClient httpClient;

    private boolean isVerified;
    private String userData;


    public LoginAsyncHttpUtil(String username, String password, Context context)
    {
        this.userName = username;
        this.password = password;
        this.context = context;

        this.httpClient = new DefaultHttpClient();

        this.isVerified = false;
        this.userData = null;
    }

    @Override
    protected Void doInBackground(Void ... voidParams)
    {
        try
        {
            String serviceURL = context.getString(R.string.URL);

            HttpGet getUserRequest = new HttpGet();
            getUserRequest.setURI(new URI(serviceURL + "/users/" + userName));
            getUserRequest.setHeader("Authorization", "Basic "+ password);

            HttpResponse response = httpClient.execute(getUserRequest);
            if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
            {
                isVerified = true;
                this.userData = StringUtils.inputStreamToString(response.getEntity().getContent());
            }
            else
            {
                isVerified = false;
            }
        }
        catch(Exception ex)
        {
            isVerified = false;
            Toast.makeText(context, "Unexpected error. Try Again.", Toast.LENGTH_SHORT).show();
        }

        return null;
    }
    @Override
    protected void onPostExecute(Void aVoid)
    {
        if(isVerified)
        {
            Intent userHomePageIntent = new Intent(context, UserHomePage.class);

            userHomePageIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            userHomePageIntent.putExtra("JSONObject", userData);

            context.startActivity(userHomePageIntent);
        }
        else
        {
            Toast.makeText(context, "Invalid Username/Password", Toast.LENGTH_SHORT).show();
        }
    }
}
