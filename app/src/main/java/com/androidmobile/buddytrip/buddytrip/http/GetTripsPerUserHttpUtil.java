package com.androidmobile.buddytrip.buddytrip.http;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidmobile.buddytrip.buddytrip.DetailTrip;
import com.androidmobile.buddytrip.buddytrip.NewTrip;
import com.androidmobile.buddytrip.buddytrip.R;
import com.androidmobile.buddytrip.buddytrip.TripUtil;
import com.androidmobile.buddytrip.buddytrip.utils.DateUtils;
import com.androidmobile.buddytrip.buddytrip.utils.LocationImageMapper;
import com.androidmobile.buddytrip.buddytrip.utils.StringUtils;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class GetTripsPerUserHttpUtil extends AsyncTask<Void, Void, Void>
{
    private final Context context;
    private final HttpClient httpClient;
    private boolean wasFetchSuccessful;
    private final String userName;
    private String userData;
    private ListView listView;
    ArrayList<TripUtil> trips;
    private Button createTripButton;
    private Integer qbId;


    public GetTripsPerUserHttpUtil(Context context, String userName, ListView list, Button createTripButton, Integer qbId)
    {
        this.context = context;
        this.httpClient = new DefaultHttpClient();
        this.wasFetchSuccessful = false;
        this.userName = userName;
        this.listView = list;
        trips = new ArrayList<>();
        this.createTripButton = createTripButton;
        this.qbId = qbId;
    }

    @Override
    protected Void doInBackground(Void ... voidParams)
    {
        try
        {
            String serviceURL = context.getString(R.string.URL);

            HttpGet getUserRequest = new HttpGet();
            getUserRequest.setURI(new URI(serviceURL + "/trips/users/" + userName));

            HttpResponse response = httpClient.execute(getUserRequest);
            if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
            {
                wasFetchSuccessful = true;
                this.userData = StringUtils.inputStreamToString(response.getEntity().getContent());
            }
            else
            {
                wasFetchSuccessful = false;
            }
        }
        catch(Exception ex)
        {
            wasFetchSuccessful = false;
        }

        return null;
    }
    @Override
    protected void onPostExecute(Void aVoid)
    {
        if(wasFetchSuccessful)
        {
            //Populate the list view with the trips
            try
            {
                if(userData.isEmpty()|| userData == null)
                {
                    Toast.makeText(context, "No trips created by you yet!!", Toast.LENGTH_SHORT).show();

                }
                else
                {
                    JSONArray jsonArray = new JSONArray(userData);
                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject object = jsonArray.getJSONObject(i);
                        Integer tripId = Integer.parseInt(object.getString("_id"));
                        Float cost = Float.parseFloat(object.getString("cost"));

                        Date dateFrom = DateUtils.getDateFromJsonObject(object.getString("dateFrom"));
                        Date dateTo = DateUtils.getDateFromJsonObject(object.getString("dateTo"));

                        if (dateFrom == null || dateTo == null)
                            continue;

                        trips.add(new TripUtil(tripId,
                                object.getString("tripTitle"),
                                object.getString("username"),
                                object.getString("location"),
                                object.getString("interest"),
                                cost,
                                dateFrom,
                                dateTo));

                    }

                    //ArrayAdapter<TripUtil> adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, trips);
                    //listView.setAdapter(adapter);

                    populateListView();


                    registerClickCallBack();
                }
            }
            catch (Exception e)
            {
                Toast.makeText(context, "Unexpected error. Try Again.", Toast.LENGTH_SHORT).show();
            }

        }
        else
        {
            Toast.makeText(context, "Unexpected error. Try Again.", Toast.LENGTH_SHORT).show();
        }

        createTripButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                final Intent createNewTripIntent =  new Intent(context, NewTrip.class);
                createNewTripIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                createNewTripIntent.putExtra("username", userName);
                try
                {
                    context.startActivity(createNewTripIntent);
                }
                catch (Exception e)
                {
                     e.printStackTrace();
                }
            }
        });
    }

    private void populateListView()
    {
        final ArrayAdapter<TripUtil> adapter = new MyListAdapter();
        listView.setAdapter(adapter);
    }

    private class MyListAdapter extends ArrayAdapter<TripUtil>
    {
        public MyListAdapter()
        {
            super(context, R.layout.triplist_view, trips);
        }

        public View getView(final int position, final View convertView, final ViewGroup parent)
        {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            //Make sure we have a view to work with
            View itemView = convertView;
            if(itemView == null)
            {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                itemView = inflater.inflate(R.layout.triplist_view,parent, false);
            }

            //Find the Trips to work with
            final TripUtil currentTrip = trips.get(position);

            final ImageView imageView = (ImageView)itemView.findViewById(R.id.item_icon);
            imageView.setImageResource(LocationImageMapper.getDrawableFor(currentTrip.getTripLocation()));

            //Trip Name
            final TextView nameText = (TextView) itemView.findViewById(R.id.item_txtName);
            nameText.setText(currentTrip.getTripTitle());


            final TextView LocationText = (TextView) itemView.findViewById(R.id.item_txtDetails);
            LocationText.setText(currentTrip.getTripInterest() + " at " + currentTrip.getTripLocation() );//+ " between " + sdf.format(currentTrip.getTripDateFrom()).toString() + " and " + sdf.format(currentTrip.getTripDateTo()).toString());

            return itemView;
        }
    }

    private void registerClickCallBack()
    {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id)
            {
                final TripUtil clickedTrip = trips.get(position);
                final int tripId = clickedTrip.getTripId();
                final String tripLocation = clickedTrip.getTripLocation();
                final String tripInterest = clickedTrip.getTripInterest();
                final String tripTitle = clickedTrip.getTripTitle();
                final Float tripCost = clickedTrip.getTripCost();
                final Date tripDateFrom = clickedTrip.getTripDateFrom();
                final Date tripDateTo = clickedTrip.getTripDateTo();
                final String username = clickedTrip.getTripUsername();


                final Intent tripDetailIntent = new Intent(context, DetailTrip.class);
                tripDetailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                tripDetailIntent.putExtra("Trip ID", tripId);
                tripDetailIntent.putExtra("Trip Location", tripLocation);
                tripDetailIntent.putExtra("Trip Interest", tripInterest);
                tripDetailIntent.putExtra("Trip Title", tripTitle);
                tripDetailIntent.putExtra("Trip Cost", tripCost);
                tripDetailIntent.putExtra("Trip Date From", tripDateFrom);
                tripDetailIntent.putExtra("Trip Date To", tripDateTo);
                tripDetailIntent.putExtra("Trip Username", username);
                tripDetailIntent.putExtra("QBId", qbId);

                context.startActivity(tripDetailIntent);
            }
        });
    }


}


