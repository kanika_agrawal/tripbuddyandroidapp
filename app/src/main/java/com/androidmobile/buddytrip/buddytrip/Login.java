package com.androidmobile.buddytrip.buddytrip;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidmobile.buddytrip.buddytrip.http.FBDataStoreInDBAsyncUtil;
import com.androidmobile.buddytrip.buddytrip.http.LoginAsyncHttpUtil;
import com.androidmobile.buddytrip.buddytrip.utils.DateUtils;
import com.androidmobile.buddytrip.buddytrip.utils.QuickBloxAccountDetails;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.quickblox.auth.QBAuth;
import com.quickblox.core.QBSettings;

import org.json.JSONObject;

import java.util.Arrays;

//import com.facebook.login.LoginC

public class Login extends ActionBarActivity
{
    private Button btn_bt_account;
    private TextView tv_create_account;
    private TextView tv_forget;
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    private EditText et_username;
    private EditText et_pwd;
    private String strUserName;
    private String strPwd;
    private boolean isConn;
    private Person user;
    private JSONObject jsIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        btn_bt_account = (Button)findViewById(R.id.btn_bt_account);
        tv_create_account = (TextView)findViewById(R.id.tv_new_account);
        et_username = (EditText)findViewById(R.id.et_username);
        et_pwd = (EditText)findViewById(R.id.et_password);
        et_username.setTextColor(Color.BLACK);
        et_pwd.setTextColor(Color.BLACK);
        isConn = false;
        jsIntent = new JSONObject();

        tv_create_account.setPaintFlags(tv_create_account.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);

        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton)findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile","email","user_birthday","user_friends"));

        QBSettings.getInstance().fastConfigInit(
                QuickBloxAccountDetails.APP_ID,
                QuickBloxAccountDetails.AUTH_KEY,
                QuickBloxAccountDetails.AUTH_SECRET);

        //using facebook login button to login
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>()
        {
            @Override
            public void onSuccess(LoginResult loginResult)
            {
                // get the user's info
                GraphRequest request = GraphRequest.newMeRequest( loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback()
                                {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response)
                                    {
                                        try
                                        {
                                            Integer age = DateUtils.getAgeFromBirthDate((String) object.get("birthday"));
                                            object.put("age", age.toString());
                                            String userId = (String) object.get("id");
                                            object.put("_id", userId);
                                            object.put("password", "");

                                            FBDataStoreInDBAsyncUtil FBDataStoreInDBAsyncUtil = new FBDataStoreInDBAsyncUtil(object, getApplicationContext());
                                            FBDataStoreInDBAsyncUtil.execute();
                                        }
                                        catch (Exception e)
                                        {
                                            Log.d("HTTPCLIENTFETCHDATA", e.getLocalizedMessage());
                                        }

                                        Log.v("LoginActivity", response.toString());
                                    }
                                });
                Bundle parameters = new Bundle();
                request.setParameters(parameters);
                request.executeAsync();
            }


            @Override
            public void onCancel()
            {
                //No necessary to implement, the app will automatically back the to main user login page
            }

            @Override
            public void onError(FacebookException error)
            {

            }
        });

        ///click the "Sign in" button to login
        btn_bt_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strUserName = et_username.getText().toString();
                strPwd = et_pwd.getText().toString();
                if(strUserName == null && strPwd == null)
                {
                    Toast.makeText(getApplicationContext(),"Please input the account!",Toast.LENGTH_SHORT).show();
                    return;
                }
                else if(strUserName == null && strPwd != null)
                {
                    Toast.makeText(getApplicationContext(),"Please input the User Name!",Toast.LENGTH_SHORT).show();
                    return;
                }
                else if(strUserName != null && strPwd == null)
                {
                    Toast.makeText(getApplicationContext(),"Please input the password!",Toast.LENGTH_SHORT).show();
                    return;
                }

                //prepare the JSON object
                Person person = new Person();
                person.setUserName(strUserName);
                person.setPassword(strPwd);

                try
                {
                    LoginAsyncHttpUtil loginUtil = new LoginAsyncHttpUtil(strUserName, strPwd, getBaseContext());

                    loginUtil.execute();
                }
                catch (Exception e)
                {
                    Log.d("HTTPCLIENTFETCHDATA", e.getLocalizedMessage());
                }
            }
        });

        tv_create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // to create a new Buddy trip account
                Intent intent = new Intent(v.getContext(), CreateAccount.class);
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
