package com.androidmobile.buddytrip.buddytrip.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kanikaagrawal on 5/20/15.
 */
public class DateUtils
{
    public static Date getDateFromJsonObject(String input)
    {
        final String patternString = "^.*:\"(.*)\".*$";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(input);

        while(matcher.find())
        {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            try
            {
                String dateString = matcher.group(1);
                Date date = sdf.parse(dateString);
                return date;
            }
            catch (ParseException e)
            {
                return null;
            }
        }

        return null;
    }

    public static Integer getAgeFromBirthDate(String birthDate)
    {
        try
        {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date birthdate = sdf.parse(birthDate);

            Calendar dob = Calendar.getInstance();
            Calendar today = Calendar.getInstance();

            dob.setTime(birthdate);
            int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

            if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR))
            {
                age--;
            }

            return age;
        }
        catch (Exception ex)
        {
            //TODO: log
        }

        return null;
    }
}
