package com.androidmobile.buddytrip.buddytrip.http;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.androidmobile.buddytrip.buddytrip.FriendList;
import com.androidmobile.buddytrip.buddytrip.Friends;
import com.androidmobile.buddytrip.buddytrip.R;
import com.androidmobile.buddytrip.buddytrip.utils.StringUtils;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import java.net.URI;
import java.util.ArrayList;

/**
 * Created by kanikaagrawal on 5/21/15.
 */
public class FindFriendsAsyncHttpUtil extends AsyncTask<Void, Void, Void>
{
    private final Context context;
    private final HttpClient httpClient;
    private final Integer tripID;
    private boolean wasFetchSuccessful;
    private String friendsData;
    ArrayList<Friends> friends;
    private String username;
    private Integer qbId;

    public FindFriendsAsyncHttpUtil(Context context,Integer tripID, String username, Integer qbId)
    {
        this.context = context;
        this.httpClient = new DefaultHttpClient();
        this.tripID = tripID;
        this.wasFetchSuccessful= false;
        this.friendsData = null;
        friends = new ArrayList<>();
        this.username = username;
        this.qbId = qbId;
    }


    @Override
    protected Void doInBackground(Void... voids)
    {
        try
        {
            String serviceURL =    context.getString(R.string.URL);
            HttpGet getFriendsRequest = new HttpGet();
            getFriendsRequest.setURI(new URI(serviceURL + "/trips/friends/" + tripID));
            HttpResponse response = httpClient.execute(getFriendsRequest);

            if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
            {
                wasFetchSuccessful = true;
                this.friendsData = StringUtils.inputStreamToString(response.getEntity().getContent());
            }
            else
            {
                wasFetchSuccessful = false;
            }

        }
        catch(Exception e)
        {
            wasFetchSuccessful = false;
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid)
    {
        if(wasFetchSuccessful)
        {
            try
            {
                JSONArray jsonArray = new JSONArray(friendsData);

                final Intent friendDetailIntent = new Intent(context, FriendList.class);
                friendDetailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                friendDetailIntent.putExtra("username", username);
                friendDetailIntent.putExtra("QBId", qbId);
                friendDetailIntent.putExtra("FriendsJsonArray", jsonArray.toString());
                context.startActivity(friendDetailIntent);
            }
            catch (Exception e)
            {
                Toast.makeText(context, "Unexpected error. Try Again.", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(context, "Unexpected error. Try Again.", Toast.LENGTH_SHORT).show();
        }
    }



}
