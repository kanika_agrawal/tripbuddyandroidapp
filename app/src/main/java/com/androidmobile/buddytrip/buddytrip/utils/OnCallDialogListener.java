package com.androidmobile.buddytrip.buddytrip.utils;

public interface OnCallDialogListener {

    public void onAcceptCallClick();
    public void onRejectCallClick();
}
