package com.androidmobile.buddytrip.buddytrip;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.androidmobile.buddytrip.buddytrip.http.GetTripsPerUserHttpUtil;

import org.json.JSONException;
import org.json.JSONObject;


public class UserHomePage extends ActionBarActivity
{
    private ImageView iv_photo;
    private TextView tv_username;
    private TextView tv_gender;
    private TextView tv_age;
    private Button btn_createTrip;
    private ListView list;
    private String name;
    private String age;
    private String gender;
    private String username;
    private Integer qbId;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userhomepage);
//        iv_photo = (ImageView)findViewById(R.id.im_photo);
        tv_username = (TextView)findViewById(R.id.tv_username);
        tv_gender = (TextView)findViewById(R.id.tv_gender);
        tv_age = (TextView)findViewById(R.id.tv_age);
        btn_createTrip = (Button)findViewById(R.id.btn_newtrip);
        list = (ListView)findViewById(R.id.list);

        //Get the data from former activity
        Intent intent = getIntent();

        if(intent != null)
        {
            try
            {
                String jsonString = intent.getStringExtra("JSONObject");
                JSONObject js = new JSONObject(jsonString);
                name = js.getString("name");
                age = js.getString("age");
                gender = js.getString("gender");
                username = js.getString("_id");
                qbId = Integer.parseInt(js.getString("QBId"));

            }
            catch (JSONException e)
            {
                //e.printStackTrace();
            }
        }

        tv_username.setText(name);
        tv_age.setText("Age: " + age);
        tv_gender.setText("Gender: " + gender);

//        GetTripsPerUserHttpUtil getTripsPerUserHttpUtil =  new GetTripsPerUserHttpUtil(getBaseContext(), username, list, btn_createTrip);
//        getTripsPerUserHttpUtil.execute();

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        GetTripsPerUserHttpUtil getTripsPerUserHttpUtil =  new GetTripsPerUserHttpUtil(getBaseContext(), username, list, btn_createTrip, qbId);
        getTripsPerUserHttpUtil.execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_userhomepage, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
